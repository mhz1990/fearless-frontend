function Nav() {
    return (
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active d-none" aria-current="page" href="new-location.html">New location</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active d-none" aria-current="page" href="new-conference.html">New conference</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="new-presentation.html">New
                    presentation</a>
            </li>
        </ul>
    );
}

export default Nav;
