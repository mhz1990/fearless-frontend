window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
            // console.log(conference)
        }

        const div_spinner = document.getElementById('loading-conference-spinner');
        div_spinner.classList.add('d-none');
        selectTag.classList.remove('d-none');
    }


    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        // console.log('need to submit the form data');
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // console.log(json);
        // console.log(conference.href)
        const select = document.getElementById('conference');
        const conference_id = select.options[select.selectedIndex].value;
        const attendeeUrl = `http://localhost:8001/api/attendees/`;
        // console.log(attendeeUrl)
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // console.log(fetchConfig)
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            // formTag.reset();
            // const newattendee = await response.json();
            // // console.log(newattendee);
            const div_success = document.getElementById('success-message');
            div_success.classList.remove('d-none');
            formTag.classList.add('d-none');
        }
    });
});
